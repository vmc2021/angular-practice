import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
  //properties go here
  // num1: number = 0;
  // num2: number = 0;
  num1: number = 0;
  num2: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
