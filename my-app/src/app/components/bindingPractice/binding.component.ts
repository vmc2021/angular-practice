import { Component, OnInit } from '@angular/core';
import {DCHeroPractice} from "../../models/DCHeroPractice";

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {
//properties
  characters: DCHeroPractice[] = []
  constructor() { }

  ngOnInit(): void {
    //add data to the array of characters
    this.characters = [
      {
        persona: "Superman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54
      }
    ]
  }

}
